package main

//Impport one more than library, take that Python
import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	fmt.Printf("Hello, World!\n")
	sayHello()
	fmt.Printf(returnToSender())
	loopExample()
	fmt.Printf(getInfo())
}

//Example of basic function and calling producer
func sayHello() {
	fmt.Printf("Called the function, sayHello\n")
}

//Example of returning vals, can also return multiple
func returnToSender() string {
	return "Hello\n"
}

//Classic for, inital, condition, increment
func loopExample() {
	for x := 0; x <= 10; x++ {
		fmt.Printf(strconv.Itoa(x))
	}
}
func getInfo() string {
	//Create a new line because looks better.
	fmt.Printf("\n")
	//Gets a Reader Buffer then Reads the console line
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	return text
}

package main

import (
	"fmt"
	"strconv"
)

func main() {
	index := readLine()
	for x := 0; x <= index; x++ {
		fmt.Println(strconv.Itoa(fib(x)))
	}

}
func fib(x int) int {
	a := 0
	b := 1
	for y := 0; y < x; y++ {
		temp := a
		a = b
		b = temp + b
	}
	return a
}
func readLine() int {
	var i int
	_, err := fmt.Scanf("%d", &i)
	//Checks to make sure it is a int
	//If it is not then will go to default = 0
	if err != nil {
		i = i
	}
	return i
}
